#!/bin/bash
# Copyright   2021   Tsinghua University (Author: Lantian Li, Yang Zhang)
# Apache 2.0.

# modified and re-distributed by Zifeng Zhao @ Peking University
# AGPL v3.0

SPEAKER_TRAINER_ROOT=..
cnceleb1_path=$1
cnceleb2_path=$2
stage=$3

if [ $stage -le 0 ];then
  # flac to wav
  python3 $SPEAKER_TRAINER_ROOT/steps/flac2wav.py \
          --dataset_dir $cnceleb1_path/data \
          --speaker_level 1

  python3 $SPEAKER_TRAINER_ROOT/steps/flac2wav.py \
          --dataset_dir $cnceleb2_path/data \
          --speaker_level 1

  python3 $SPEAKER_TRAINER_ROOT/steps/flac2wav.py \
          --dataset_dir $cnceleb1_path/eval/enroll \
          --speaker_level 0
  
  python3 $SPEAKER_TRAINER_ROOT/steps/flac2wav.py \
          --dataset_dir $cnceleb1_path/eval/test \
          --speaker_level 0
fi