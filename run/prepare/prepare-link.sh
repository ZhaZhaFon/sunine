#!/bin/bash
# Copyright   2021   Tsinghua University (Author: Lantian Li, Yang Zhang)
# Apache 2.0.

# modified and re-distributed by Ji Jiang, Zifeng Zhao @ Peking University
# AGPL v3.0

cnceleb1_path=$1
cnceleb2_path=$2
stage=$3

if [ $stage -eq 2 ];then
  # prepare data
  if [ ! -d data/wav ]; then
    mkdir -p data/wav
  fi

  mkdir -p data/wav/train
  for spk in `cat ${cnceleb1_path}/dev/dev.lst`; do
    ln -s ${cnceleb1_path}/data/${spk} data/wav/train/$spk
  done

  for spk in `cat ${cnceleb2_path}/spk.lst`; do
    ln -s ${cnceleb2_path}/data/${spk} data/wav/train/$spk
  done

  # prepare evaluation trials
  mkdir -p data/trials
  python3 local/format_trials_cnceleb.py \
          --cnceleb_root $cnceleb1_path \
          --dst_trl_path data/trials/CNC-Eval-Core.lst
fi

echo done.