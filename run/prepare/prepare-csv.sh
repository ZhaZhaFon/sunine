#!/bin/bash
# Copyright   2021   Tsinghua University (Author: Lantian Li, Yang Zhang)
# Apache 2.0.

# modified and re-distributed by Zifeng Zhao @ Peking University
# AGPL v3.0

SPEAKER_TRAINER_ROOT=..
stage=$1

if [ $stage -eq 3 ];then
  # prepare data for model training
  mkdir -p data
  echo Build train list
  python3 $SPEAKER_TRAINER_ROOT/steps/build_datalist.py \
          --data_dir data/wav/train \
          --extension wav \
          --speaker_level 1 \
          --data_list_path data/train_lst.csv
fi

echo done.