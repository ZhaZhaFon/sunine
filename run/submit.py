
# python submit.py --score_file path/to/score_file.score.foo
# output: /path/to/score_file.score -> submit to CNSRC

import pandas as pd
import argparse
parser = argparse.ArgumentParser(description='for sumbission')
parser.add_argument('--score_file', type=str, required=True, help='path to score file')
args = parser.parse_args()

import tqdm

if __name__ == '__main__':
    submission_name = args.score_file.split('.ckpt')[0] + '.score'
    score_input = pd.read_csv(args.score_file, sep=' ', header=None)
    
    with open(submission_name, 'w') as f:
        for idx in tqdm.tqdm(range(len(score_input))):

            gt = score_input[0][idx]
            enroll = score_input[1][idx]
            test = score_input[2][idx]
            score = score_input[3][idx]
            
            new_line_0 = enroll.split('/')[-1].split('-')[0]
            new_line_1 = test.split('/')[-1].split('.')[0]
            new_line_2 = score

            new_line = f'{new_line_0} {new_line_1} {new_line_2}\n'
            f.write(new_line)
        
        
        
