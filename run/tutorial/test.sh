

stage=$1
exp_name=$2

# training
batch_size=256
max_epochs=100
learning_rate=0.01
lr_step_size=5
lr_gamma=0.9

# monitor
cuda_device=0
eval_interval=10
save_top_k=20

# model
nnet_type=ResNet34L
pooling_type=TSP

# loss
loss_type=amsoftmax
embedding_dim=256
scale=30.0
margin=0.1
nPerSpeaker=1

if [ $stage -eq 5 ];then
    mkdir -p /home/zzf/experiment-sunine/${exp_name}/scores
    for ckpt_path in /home/zzf/experiment-sunine/${exp_name}/*.ckpt; do
        echo ''
        echo ckpt_path = $ckpt_path
        CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore main.py \
                --exp_name /home/zzf/experiment-sunine/$exp_name \
                --evaluate \
                --checkpoint_path $ckpt_path \
                --n_mels 80 \
                --scores_path /home/zzf/experiment-sunine/$exp_name/scores/CNC-Eval-Core.foo \
                --apply_metric \
                --nnet_type $nnet_type \
                --pooling_type $pooling_type \
                --scale $scale \
                --margin $margin \
                --num_workers 8 \
                --gpus 1 \
                --apply_plda \
                --plda_dim 128 \
                --dev_list_path data/train_lst.csv
    done
fi




