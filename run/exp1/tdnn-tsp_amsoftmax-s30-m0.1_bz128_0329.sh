#!/bin/bash
# Copyright   2021   Tsinghua University (Author: Lantian Li, Yang Zhang)
# Apache 2.0.

# modified and re-distributed by Zifeng Zhao @ Peking University
# AGPL v3.0

# inherit from ecapa-tsp_amsoftmax-s30-m0.1_bz128_0329.sh
exp_name=tdnn-tsp_amsoftmax-s30-m0.1_bz128_0329
cuda_device=2

# training
batch_size=128
max_epochs=65
learning_rate=0.01
lr_step_size=5
lr_gamma=0.9

# monitor
eval_interval=1
save_top_k=5

# model
nnet_type=TDNN # TODO
pooling_type=TSP

# loss
loss_type=amsoftmax
embedding_dim=256
scale=30.0
margin=0.1
nPerSpeaker=1

# model training
stage=4
if [ $stage -eq 4 ];then
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore main_2.py \
          --exp_name $exp_name \
          --n_mels 80 \
          --max_frames 201 --min_frames 200 \
          --batch_size $batch_size \
          --nPerSpeaker $nPerSpeaker \
          --max_seg_per_spk 500 \
          --num_workers 8 \
          --max_epochs $max_epochs \
          --loss_type $loss_type \
          --nnet_type $nnet_type \
          --pooling_type $pooling_type \
          --embedding_dim $embedding_dim \
          --learning_rate $learning_rate \
          --lr_step_size $lr_step_size \
          --lr_gamma $lr_gamma \
          --margin $margin \
          --scale $scale \
          --eval_interval $eval_interval \
          --eval_frames 0 \
          --scores_path tmp.foo \
          --apply_metric \
          --save_top_k $save_top_k \
          --distributed_backend dp \
          --reload_dataloaders_every_epoch \
          --gpus 1
fi

# model testing
# inherit from test.sh
stage=5
if [ $stage -eq 5 ];then
    for ckpt_path in /home/zzf/experiment-sunine/${exp_name}/*.ckpt; do
        echo ''
        echo ckpt_path = $ckpt_path
        CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore main_2.py \
                --exp_name /home/zzf/experiment-sunine/$exp_name \
                --evaluate \
                --checkpoint_path $ckpt_path \
                --n_mels 80 \
                --scores_path ${ckpt_path}.score.foo \
                --apply_metric \
                --nnet_type $nnet_type \
                --pooling_type $pooling_type \
                --scale $scale \
                --margin $margin \
                --num_workers 8 \
                --gpus 1
    done
fi

echo ''
echo '### EXPERIMENT COMPLETED ###'
echo '### EXPERIMENT COMPLETED ###'
echo '### EXPERIMENT COMPLETED ###'
echo ''