

test_folder=/home/zzf/experiment-sunine/config-super-res34/res34-tsp_aamsoftmax-s30-m0.1_bz128_0331
exp_name=res34-tsp_aamsoftmax-s30-m0.1_bz128_0331

# monitor
cuda_device=2

# model
nnet_type=ResNet34
pooling_type=TSP

# loss
embedding_dim=256

stage=5
if [ $stage -eq 5 ];then
    for ckpt_path in ${test_folder}/*.ckpt; do
        echo ''
        echo ckpt_path = $ckpt_path
        CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore main_2.py \
                --exp_name /home/zzf/experiment-sunine/config-super-res34/$exp_name \
                --evaluate \
                --checkpoint_path $ckpt_path \
                --n_mels 80 \
                --scores_path ${ckpt_path}.score.foo \
                --apply_metric \
                --nnet_type $nnet_type \
                --pooling_type $pooling_type \
                --num_workers 8 \
                --gpus 1
    done
fi




